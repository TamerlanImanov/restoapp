//
//  Service.m
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "Service.h"
#import "Pivot.h"

@implementation Service

-(instancetype)initWithDictionary:(NSDictionary*) dictionary{
    self = [super init];
    
    if (self) {
        self.serviceId = dictionary[@"id"];
        self.createdAt = dictionary[@"created_at"];
        self.name = dictionary[@"name"];
        self.updatedAt = dictionary[@"updated_at"];
        self.pivot = [[Pivot alloc] initWithDictionary:dictionary[@"pivot"]];
    }
    
    return self;
}

@end
