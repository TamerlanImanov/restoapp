
#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
@class MyNSURLConnection;

@protocol MyNSURLConnectionDelegate <NSObject>

// В случае успешного запроса
-(void) downloadConnection:(MyNSURLConnection*)connection finishWithData:(NSMutableData*) data;

//  В случае когда запрос не удался
//  Можно использовать [error localizedDescription] для отображения ошибки
-(void) downloadConnection:(MyNSURLConnection*)connection failWithError:(NSError *)error;

@end

@interface MyNSURLConnection : NSObject<NSURLConnectionDataDelegate>

@property (strong, nonatomic) id <MyNSURLConnectionDelegate> delegate;

//  Можно добавить UIProgressView для визуального отображения, на каком этапе запрос или указать nil
- (id)initWithUrl:(NSString*)stringUrl progress:(UIProgressView*)ProgressView;

//  Для POST запросов
//  Также вызывает метод Start.
//  Так, что не надо после этого метода вызывать еще и start
- (void)postRequest:(NSDictionary*)postValues;

//  для GET запросов
//  Также вызывает метод Start.
//  Так, что не надо после этого метода вызывать еще и start
- (void)getRequest:(NSDictionary*)getValues;

//  Отправляет запрос 
- (void)start;

//  Останавливает запрос
- (void)stop;

@end
