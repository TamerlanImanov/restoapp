//
//  Kitchen.m
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "Kitchen.h"
#import "Pivot.h"

@implementation Kitchen

-(instancetype)initWithDictionary:(NSDictionary*) dictionary{
    self = [super init];
    
    if (self) {
        self.kitchenId = dictionary[@"id"];
        self.createdAt = dictionary[@"created_at"];
        self.name = dictionary[@"name"];
        self.updatedAt = dictionary[@"updated_at"];
        self.pivot = [[Pivot alloc] initWithDictionary:dictionary[@"pivot"]];
    }
    
    return self;
}


@end
