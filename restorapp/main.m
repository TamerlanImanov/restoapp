//
//  main.m
//  restorapp
//
//  Created by Tamerlan Imanov on 17.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
