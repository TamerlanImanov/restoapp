//
//  PersonalViewController.h
//  restorapp
//
//  Created by Tamerlan Imanov on 19.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
@interface PersonalViewController : UIViewController <MKMapViewDelegate>{

}
@property (weak, nonatomic) IBOutlet UIImageView *personalImage;
@property (weak, nonatomic) IBOutlet UILabel *personalName;
@property (weak, nonatomic) IBOutlet UILabel *personalKitchen;
@property (weak, nonatomic) IBOutlet UILabel *personalAddress;
@property (weak, nonatomic) IBOutlet UILabel *personalWork;
@property (weak, nonatomic) IBOutlet UILabel *personalBills;
@property (weak, nonatomic) IBOutlet UITextView *personalDesc;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *personalVIew;
@property (weak, nonatomic) IBOutlet UILabel *personalPhoneLabel;
@property (weak, nonatomic) IBOutlet UILabel *personalSite;
@property (weak, nonatomic) IBOutlet UIButton *callButton;
- (IBAction)callAction:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *callLabel;
@property (weak, nonatomic) UIImage *personalImageGet;
@property (nonatomic, strong) NSDictionary *personalDic;
@end
