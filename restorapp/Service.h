//
//  Service.h
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Pivot;

@interface Service : NSObject

@property (strong,nonatomic) NSString * createdAt;
@property (strong,nonatomic) NSString * serviceId;
@property (strong,nonatomic) NSString * updatedAt;
@property (strong,nonatomic) NSString * name;
@property (strong,nonatomic) Pivot * pivot;

-(instancetype)initWithDictionary:(NSDictionary*) dictionary;

@end
