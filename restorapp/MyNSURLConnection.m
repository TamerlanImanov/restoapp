
#import "MyNSURLConnection.h"

@interface MyNSURLConnection ()

@property (strong, nonatomic) NSMutableData * receivedData;
@property (copy, nonatomic) NSString * currentURL;
@property (assign, nonatomic) float expectedBytes;
@property (strong, nonatomic) UIProgressView *progress;
@property (strong, nonatomic) NSURLConnection * connection;

@end

@implementation MyNSURLConnection

//==================================
#pragma mark - Init
//==================================

-(id)initWithUrl:(NSString*)stringUrl progress:(UIProgressView*)ProgressView{
    self.currentURL=stringUrl;
    self.progress = ProgressView;
    return [super init];
}


//==================================
#pragma mark - Requests
//==================================


-(void)postRequest:(NSDictionary*)postValues{
    [self postRequestWithString:[self toStrFromDict:postValues]];
}


-(void)postRequestWithString:(NSString*)postValues{
    NSString * post = postValues;
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:self.currentURL] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    [request setHTTPMethod:@"POST"];
    [request setHTTPShouldHandleCookies:YES];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding ];
    [ request setHTTPBody: postData ];
    
    self.receivedData = [[NSMutableData alloc] initWithLength:0];
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}


-(void)getRequest:(NSDictionary*)getValues{
    NSMutableURLRequest *request;
    if(getValues){
         NSString * get = [self toStrFromDict:getValues];
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@?%@",self.currentURL,get]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    }
    else{
        request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString: [NSString stringWithFormat:@"%@",self.currentURL]] cachePolicy:NSURLRequestReloadIgnoringLocalCacheData timeoutInterval:60];
    }
   
    self.receivedData = [[NSMutableData alloc] initWithLength:0];
    
    self.connection = [[NSURLConnection alloc] initWithRequest:request delegate:self startImmediately:YES];
}


//==================================
#pragma mark - Convert NSDictionary to NSString for request
//==================================


-(NSString*) toStrFromDict:(NSDictionary*)values{
    NSMutableString * string = [[NSMutableString alloc] init];
    NSArray * keys = [values allKeys];
    for (int i = 0; i<keys.count; i++) {
        NSString * key = keys[i];
        
        if ([values[key] isKindOfClass:[NSArray class]] || [values[key] isKindOfClass:[NSMutableArray class]]){
            NSArray * v = values[key];
            for (int j = 0; j<v.count; j++) {
                if ([v[j] isKindOfClass:[NSDictionary class]] || [v[j] isKindOfClass:[NSMutableDictionary class]]) {
                    [string appendFormat:@"%@", [self toStrFromDict:v[j]]];
                }
                else {
                    [string appendFormat:@"%@=%@",key, v[j]];
                }
                if (j<v.count-1 )[string appendFormat:@"&"];
            }
        }
        else if ([values[key] isKindOfClass:[NSDictionary class]] || [values[key] isKindOfClass:[NSMutableDictionary class]]) {
            [string appendFormat:@"%@", [self toStrFromDict:values[key]]];
        }
        else {
            [string appendFormat:@"%@=%@",key, values[key]];
        }
        if (i<keys.count-1)[string appendFormat:@"&"];
    }
    
    return string;
}


//==================================
#pragma mark - Start/Stop
//==================================


-(void)start{
    [self.connection start];
}

-(void) stop{
    [self.connection cancel];
}


//==================================
#pragma mark - Connection
//==================================


- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    self.progress.hidden = NO;
    [self.receivedData setLength:0];
    self.expectedBytes = [response expectedContentLength];
}


- (void) connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
    [self.receivedData appendData:data];
    float progressive = (float)[self.receivedData length] / (float)self.expectedBytes;
    [self.progress setProgress:progressive];
}


- (void) connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    [self.delegate downloadConnection:self failWithError:error];
}


- (NSCachedURLResponse *) connection:(NSURLConnection *)connection willCacheResponse:(NSCachedURLResponse *)cachedResponse {
    return nil;
}


- (void) connectionDidFinishLoading:(NSURLConnection *)connection {
    [self.delegate downloadConnection:self finishWithData:self.receivedData];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    self.progress.hidden = YES;
}

@end
