//
//  ListCell.m
//  restorapp
//
//  Created by Tamerlan Imanov on 18.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "ListCell.h"

@implementation ListCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
