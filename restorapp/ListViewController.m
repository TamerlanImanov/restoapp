//
//  ListViewController.m
//  restorapp
//
//  Created by Tamerlan Imanov on 18.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "ListViewController.h"
#import "MyNSURLConnection.h"
#import "ListCell.h"
#import "PersonalViewController.h"
#import "AppDelegate.h"
#import "Restaurant.h"

@interface ListViewController () <MyNSURLConnectionDelegate>

@end

@implementation ListViewController{
    NSArray *json;
    NSMutableArray *json2;
    NSString * documentsDirectory;
}


-(NSMutableArray *)finalArray{
    _finalArray= [[[NSUserDefaults standardUserDefaults] objectForKey:@"restor"] mutableCopy];
    if (!_finalArray) {
        _finalArray = [[NSMutableArray alloc] init];
    }
    return _finalArray;
}

-(void)viewWillAppear:(BOOL)animated{
    MyNSURLConnection* connection = [[MyNSURLConnection alloc] initWithUrl:@"http://resto.rocketeer.kz/api/resto/all/city=almaty" progress:nil];
    NSDictionary *myDic = @{};
    [connection getRequest:myDic];
    connection.delegate=self;

}

-(void)loadMethod{
    [self.activityIndicator stopAnimating];
    self.tableView.hidden = NO;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    documentsDirectory = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    [self.activityIndicator stopAnimating];
//    //    self.tableView.hidden = YES;
    //    [self performSelector:@selector(loadMethod) withObject:nil afterDelay:5.0f];
    for (UIButton* but in self.whiteView.subviews){
        [[but layer] setBorderWidth:2.0f];
        [[but layer] setCornerRadius:8.0f];
        [[but layer] setBorderColor:[UIColor lightGrayColor].CGColor];
    }
    [self changeSelectedButton:(self.restBut)];
    
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(NSMutableDictionary*)saveInNSUserDefault:(NSDictionary*)myItem{
    NSMutableDictionary *restoraunts = [[NSMutableDictionary alloc] init];
    [restoraunts setObject:[myItem valueForKey:@"id"] forKey:@"id"];
    [restoraunts setObject:[myItem valueForKey:@"resto_name"] forKey:@"resto_name"];
    [restoraunts setObject:[myItem valueForKey:@"resto_desc"] forKey:@"resto_desc"];
    [restoraunts setObject:[myItem valueForKey:@"resto_desc"] forKey:@"resto_desc"];
    [restoraunts setObject:[myItem valueForKey:@"lon"] forKey:@"lon"];
    [restoraunts setObject:[myItem valueForKey:@"lat"] forKey:@"lat"];
    [restoraunts setObject:[myItem valueForKey:@"average_bills"] forKey:@"average_bills"];
    [restoraunts setObject:[myItem valueForKey:@"contact_phone"] forKey:@"contact_phone"];
    [restoraunts setObject:[myItem valueForKey:@"work_in"] forKey:@"work_in"];
    [restoraunts setObject:[myItem valueForKey:@"work_to"] forKey:@"work_to"];
    [restoraunts setObject:[myItem valueForKey:@"address"] forKey:@"address"];
    [restoraunts setObject:[myItem valueForKey:@"city"] forKey:@"city"];
    [restoraunts setObject:[myItem valueForKey:@"type"] forKey:@"type"];
    //        [restoraunts setObject:[myItem valueForKey:@"kitchen"] forKey:@"kitchen"];
    [restoraunts setObject:[myItem valueForKey:@"district"] forKey:@"district"];
    [self.tableView reloadData];
    return restoraunts;
}

-(void)downloadConnection:(MyNSURLConnection *)connection finishWithData:(NSMutableData *)data{

    NSError *err = nil;
    json = [NSJSONSerialization
                JSONObjectWithData:data
                options:kNilOptions
                error:&err];
    
    NSMutableArray * restorauntsFromRequest = [[NSMutableArray alloc]init];

    for (NSDictionary *dic in json){
        Restaurant * restaurant = [[Restaurant alloc] initWithDictionary:dic];
        [restorauntsFromRequest addObject:restaurant];
//        NSLog(@"%@",restaurant);
    }
    
    [self.tableView reloadData];
//    [[NSUserDefaults standardUserDefaults] setObject:restorauntsArray forKey:@"restor"];

    
}

-(void)saveImage:(UIImage *)image withFileName:(NSString *)imageName ofType:(NSString *)extension inDirectory:(NSString *)directoryPath {
    if ([[extension lowercaseString] isEqualToString:@"png"]) {
        [UIImagePNGRepresentation(image) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"png"]] options:NSAtomicWrite error:nil];
    } else if ([[extension lowercaseString] isEqualToString:@"jpg"] || [[extension lowercaseString] isEqualToString:@"jpeg"]) {
        [UIImageJPEGRepresentation(image, 1.0) writeToFile:[directoryPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.%@", imageName, @"jpg"]] options:NSAtomicWrite error:nil];
    } else {
        NSLog(@"Image Save Failed\nExtension: (%@) is not recognized, use (PNG/JPG)", extension);
    }
}

-(UIImage*)loadFileFromDocumentFolder:(NSString *) filename {
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory,    NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    NSString *outputPath = [documentsDirectory stringByAppendingPathComponent:filename ];
    
    NSLog(@"outputPath: %@", outputPath);
    UIImage *theImage = [UIImage imageWithContentsOfFile:outputPath];
    
    if (theImage) {
        return theImage;
    }
    return nil;
}

-(void)downloadConnection:(MyNSURLConnection *)connection failWithError:(NSError *)error{
    //    NSLog(@"downloadConnection error = %@",error);
    
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.finalArray count];
}

-(void)checkPlaceholder:(UILabel*)lab withInt:(int)labID{
    //    [lab setTextColor:[UIColor lightGrayColor]];
    if (labID==1) {
        lab.text = @"Название";
    }else if (labID){
        lab.text = @"Адрес";
    }else{
        lab.text = @"Кухня";
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary * myTable = self.finalArray[indexPath.row];
    NSString * nameImage = [NSString stringWithFormat:@"%@.png",[myTable valueForKey:@"id"]];
    cell.imageCell.image = [self loadFileFromDocumentFolder:nameImage];
    cell.nameCell.text =   [myTable valueForKey:@"resto_name"];
    cell.addressCell.text = [myTable valueForKey:@"address"];
//        cell=tabletCell;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    //    Change the selected background view of the cell.
    [self performSegueWithIdentifier:@"next" sender:self];
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier isEqualToString:@"next"]){
        NSIndexPath* indexPath = [self.tableView indexPathForSelectedRow];
        NSMutableDictionary* d =[self.finalArray objectAtIndex:indexPath.row];
        NSString * nameImage = [NSString stringWithFormat:@"%@.png",[d valueForKey:@"id"]];
        // Get reference to the destination view controller
        PersonalViewController *vc = [segue destinationViewController];
        
        // Pass any objects to the view controller here, like...
        vc.personalDic = d;
        vc.personalImageGet =[self loadFileFromDocumentFolder:nameImage];
        //        [self.tableView reloadData];
    }
}

- (IBAction)changeList:(UIButton *)sender {
    [self dontChangeButton:sender withTag:[sender tag]];
    if ([sender tag]==1){
        [self changeSelectedButton:sender];
    }else if ([sender tag]==2){
        [self changeSelectedButton:sender];
    }else if ([sender tag]==3){
        [self changeSelectedButton:sender];
    }else{
        [self changeSelectedButton:sender];
    }
}

-(void)changeSelectedButton:(UIButton*)but{
    [[but layer] setBackgroundColor:[UIColor redColor].CGColor];
    [but setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
}


-(void)dontChangeButton:(UIButton*)but withTag:(int)intOfTag{
    for (UIButton* but in self.whiteView.subviews){
        if ([but tag]!=intOfTag){
            [[but layer] setBackgroundColor:[UIColor whiteColor].CGColor];
            [but setTitleColor:[UIColor lightGrayColor] forState:UIControlStateNormal];
        }
    }
}

@end