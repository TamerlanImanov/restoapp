//
//  Pivot.m
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "Pivot.h"

@implementation Pivot

-(instancetype)initWithDictionary:(NSDictionary*) dictionary{
    self = [super init];
    
    if (self) {
        self.createdAt = dictionary[@"created_at"];
        self.restoId = dictionary[@"resto_id"];
        self.updatedAt = dictionary[@"updated_at"];
    }
    
    return self;
}


@end
