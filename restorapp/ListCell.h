//
//  ListCell.h
//  restorapp
//
//  Created by Tamerlan Imanov on 18.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imageCell;
@property (weak, nonatomic) IBOutlet UILabel *nameCell;
@property (weak, nonatomic) IBOutlet UILabel *kitchenCell;
@property (weak, nonatomic) IBOutlet UILabel *addressCell;

@end
