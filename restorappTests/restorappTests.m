//
//  restorappTests.m
//  restorappTests
//
//  Created by Tamerlan Imanov on 17.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <XCTest/XCTest.h>
#import "Restaurant.h"

@interface restorappTests : XCTestCase

@property (copy, nonatomic) NSString * json;

@end

@implementation restorappTests

-(NSString *)json{
    return @"[{\"id\":\"32\",\"resto_name\":\"Rampapap\",\"resto_desc\":\" descriptions\r\n \",\"banner\":\"\/cd\/de\/rs.png\",\"lon\":\"0.000000\",\"lat\":\"0.000000\",\"average_bills\":\"\u0434\u043e 2500\",\"contact_phone\":\"7 (894) 561-234\",\"work_in\":\"09:30:00\",\"created_at\":\"2015-08-31 14:00:52\",\"updated_at\":\"2015-09-03 19:19:10\",\"work_to\":\"09:30:00\",\"address\":\"asdasd st 253\",\"city\":\"almaty\",\"type\":\"\u0420\u0430\u0437\u0432\u043b\u0435\u043a\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u043a\u043e\u043c\u043f\u043b\u0435\u043a\u0441\",\"district\":\"\u0410\u043b\u043c\u0430\u043b\u0438\u043d\u0441\u043a\u0438\u0439\",\"kitchens\":[{\"id\":\"3\",\"name\":\"\u0410\u0437\u0435\u0440\u0431\u0430\u0439\u0434\u0436\u0430\u043d\u0441\u043a\u0430\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"32\",\"kitchen_id\":\"3\",\"created_at\":\"2015-08-31 14:00:52\",\"updated_at\":\"2015-08-31 14:00:52\"}},{\"id\":\"6\",\"name\":\"\u0410\u043d\u0433\u043b\u0438\u0439\u0441\u043a\u0430\u044f \u043a\u0443\u0445\u043d\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"32\",\"kitchen_id\":\"6\",\"created_at\":\"2015-09-03 19:19:10\",\"updated_at\":\"2015-09-03 19:19:10\"}}],\"services\":[{\"id\":\"4\",\"name\":\"\u0414\u0435\u0442\u0441\u043a\u0430\u044f \u043a\u043e\u043c\u043d\u0430\u0442\u0430\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"32\",\"service_id\":\"4\",\"created_at\":\"2015-09-03 19:19:10\",\"updated_at\":\"2015-09-03 19:19:10\"}},{\"id\":\"6\",\"name\":\"\u0421\u0443\u0448\u0438 \u043c\u0435\u043d\u044e\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"32\",\"service_id\":\"6\",\"created_at\":\"2015-09-03 19:19:10\",\"updated_at\":\"2015-09-03 19:19:10\"}}]},{\"id\":\"24\",\"resto_name\":\"Devyatka\",\"resto_desc\":\" \u00abDevyatka\u00bb \u2014 \u044d\u0442\u043e \u0441\u043f\u043e\u0440\u0442-\u0431\u0430\u0440, \u0440\u0430\u0441\u043f\u043e\u043b\u043e\u0436\u0435\u043d\u043d\u044b\u0439 \u0432 \u0410\u043b\u043c\u0430\u043b\u0438\u043d\u0441\u043a\u043e\u043c \u0440\u0430\u0439\u043e\u043d\u0435 \u0433\u043e\u0440\u043e\u0434\u0430 \u0410\u043b\u043c\u0430\u0442\u044b. \u0413\u043e\u0441\u0442\u0435\u0439 \u0437\u0434\u0435\u0441\u044c \u0436\u0434\u0443\u0442 \u0441\u043f\u043e\u0440\u0442\u0438\u0432\u043d\u044b\u0435 \u0442\u0440\u0430\u043d\u0441\u043b\u044f\u0446\u0438\u0438 \u0432 HD-\u043a\u0430\u0447\u0435\u0441\u0442\u0432\u0435, \u0430\u0440\u043e\u043c\u0430\u0442\u043d\u044b\u0439 \u043a\u043e\u0444\u0435, \u0430\u043b\u043a\u043e\u0433\u043e\u043b\u044c\u043d\u044b\u0435 \u043d\u0430\u043f\u0438\u0442\u043a\u0438, \u0441\u0432\u0435\u0436\u0435\u0435 \u043f\u0438\u0432\u043e, \u0430\u043f\u043f\u0435\u0442\u0438\u0442\u043d\u044b\u0435 \u0437\u0430\u043a\u0443\u0441\u043a\u0438 \u0438 \u0432\u0435\u0436\u043b\u0438\u0432\u043e\u0435 \u043e\u0431\u0441\u043b\u0443\u0436\u0438\u0432\u0430\u043d\u0438\u0435. \",\"banner\":\"http:\/\/p0.zoon.ru\/preview\/8YlTKin3ZrGleLTrQR7DJg\/520x270x85\/0\/1\/a\/540d5a9d40c08805578b4571_54190b3831964.jpg\",\"lon\":\"43.254810\",\"lat\":\"76.937531\",\"average_bills\":\"10000-15000\",\"contact_phone\":\"7 (701) 099-997\",\"work_in\":\"12:00:00\",\"created_at\":\"2015-08-15 11:15:27\",\"updated_at\":\"2015-09-03 19:22:40\",\"work_to\":\"03:00:00\",\"address\":\"\u041a\u0430\u0437\u044b\u0431\u0435\u043a \u0431\u0438, 85, \u0443\u0433\u043e\u043b \u0443\u043b. \u041d\u0430\u0443\u0440\u044b\u0437\u0431\u0430\u0439 \u0431\u0430\u0442\u044b\u0440\u0430 \",\"city\":\"almaty\",\"type\":\"\u0421\u043f\u043e\u0440\u0442 \u0431\u0430\u0440\",\"district\":\"\u0410\u043b\u043c\u0430\u043b\u0438\u043d\u0441\u043a\u0438\u0439\",\"kitchens\":[{\"id\":\"28\",\"name\":\"\u041c\u0435\u0441\u0442\u043d\u0430\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"24\",\"kitchen_id\":\"28\",\"created_at\":\"2015-09-03 19:22:40\",\"updated_at\":\"2015-09-03 19:22:40\"}}],\"services\":[{\"id\":\"1\",\"name\":\"\u0424\u0443\u0442\u0431\u043e\u043b\u044c\u043d\u044b\u0435 \u0442\u0440\u0430\u043d\u0441\u043b\u044f\u0446\u0438\u0438\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"24\",\"service_id\":\"1\",\"created_at\":\"2015-09-03 19:22:40\",\"updated_at\":\"2015-09-03 19:22:40\"}},{\"id\":\"13\",\"name\":\"\u041a\u0440\u0443\u0433\u043b\u043e\u0441\u0443\u0442\u043e\u0447\u043d\u043e\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"24\",\"service_id\":\"13\",\"created_at\":\"2015-09-03 19:22:40\",\"updated_at\":\"2015-09-03 19:22:40\"}}]},{\"id\":\"23\",\"resto_name\":\"\u0420\u0435\u0441\u0442\u043e\u0440\u0430\u043d \u0410\u043b\u0430\u0448\u0430\",\"resto_desc\":\" \u0410\u043b\u0430\u0448\u0430 - \u043a\u043e\u0432\u0435\u0440 \u0440\u0443\u0447\u043d\u043e\u0439 \u0440\u0430\u0431\u043e\u0442\u044b, \u043f\u043e \u0432\u043e\u0441\u0442\u043e\u0447\u043d\u043e\u043c\u0443 \u043f\u0440\u0435\u0434\u0430\u043d\u0438\u044e, \u043f\u0440\u0435\u0434\u043d\u0430\u0437\u043d\u0430\u0447\u0435\u043d\u0430 \u0434\u043b\u044f \u0434\u043e\u0440\u043e\u0433\u0438\u0445 \u0438 \u0436\u0435\u043b\u0430\u043d\u043d\u044b\u0445 \u0433\u043e\u0441\u0442\u0435\u0439. \u0413\u043e\u0441\u0442\u0435\u0439, \u043f\u0440\u0438\u0445\u043e\u0434\u044f\u0449\u0438\u0445 \u0432 \u0434\u043e\u043c, \u0436\u0434\u0435\u0442 \u043e\u0431\u044f\u0437\u0430\u0442\u0435\u043b\u044c\u043d\u044b\u0439 \u0434\u0443\u0448\u0438\u0441\u0442\u044b\u0439 \u0447\u0430\u0439 \u0438 \u0432\u0441\u0435\u0432\u043e\u0437\u043c\u043e\u0436\u043d\u044b\u0435 \u044f\u0441\u0442\u0432\u0430 \u0432\u043e\u0441\u0442\u043e\u0447\u043d\u043e\u0439 \u043a\u0443\u0445\u043d\u0438. \u0412\u043e\u043b\u0448\u0435\u0431\u043d\u0438\u043a\u0438 \u043a\u0443\u0445\u043d\u0438 \u043d\u0430\u0448\u0435\u0433\u043e \u0440\u0435\u0441\u0442\u043e\u0440\u0430\u043d\u0430 \",\"banner\":\"http:\/\/www.alasha.kz\/upload\/14.jpg\",\"lon\":\"43.202164\",\"lat\":\"76.976280\",\"average_bills\":\"\u0441\u0432\u044b\u0448\u0435 15000\",\"contact_phone\":\"7 (727) 254-070\",\"work_in\":\"09:30:00\",\"created_at\":\"2015-08-15 11:12:03\",\"updated_at\":\"2015-09-03 19:22:08\",\"work_to\":\"09:30:00\",\"address\":\"\u0433.\u0410\u043b\u043c\u0430\u0442\u044b \u0443\u043b. \u041c.\u041e\u0441\u043f\u0430\u043d\u043e\u0432\u0430, 20\",\"city\":\"almaty\",\"type\":\"\u0420\u0435\u0441\u0442\u043e\u0440\u0430\u043d\",\"district\":\"\u041c\u0435\u0434\u0435\u0443\u0441\u043a\u0438\u0439\",\"kitchens\":[{\"id\":\"2\",\"name\":\"\u0410\u0432\u0442\u043e\u0440\u0441\u043a\u0430\u044f \u043a\u0443\u0445\u043d\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"23\",\"kitchen_id\":\"2\",\"created_at\":\"2015-09-03 19:22:08\",\"updated_at\":\"2015-09-03 19:22:08\"}}],\"services\":[{\"id\":\"2\",\"name\":\"\u0411\u0430\u043d\u0435\u0442\u043d\u044b\u0439 \u0437\u0430\u043b\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"23\",\"service_id\":\"2\",\"created_at\":\"2015-09-03 19:22:08\",\"updated_at\":\"2015-09-03 19:22:08\"}}]},{\"id\":\"20\",\"resto_name\":\"\u0410\u043b\u044b\u0435 \u043f\u0430\u0440\u0443\u0441\u0430\",\"resto_desc\":\" \u0421\u0443\u0448\u0438 \u0431\u0430\u0440\r\n \",\"banner\":\"http:\/\/www.bankoboev.ru\/images\/MzE5NjQ4\/Bankoboev.Ru_alye_parusa_v_otkrytom_more.jpg\",\"lon\":\"43.284412\",\"lat\":\"76.912201\",\"average_bills\":\"2500-7000\",\"contact_phone\":\"1 (234) 567-898\",\"work_in\":\"09:30:00\",\"created_at\":\"2015-08-05 16:47:09\",\"updated_at\":\"2015-09-03 19:21:52\",\"work_to\":\"09:30:00\",\"address\":\"\u0410\u0414\u041a\",\"city\":\"almaty\",\"type\":\"\u041f\u0438\u0446\u0446\u0435\u0440\u0438\u044f\",\"district\":\"\u0410\u0443\u044d\u0437\u043e\u0432\u0441\u043a\u0438\u0439\",\"kitchens\":[{\"id\":\"28\",\"name\":\"\u041c\u0435\u0441\u0442\u043d\u0430\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"20\",\"kitchen_id\":\"28\",\"created_at\":\"2015-09-03 19:21:52\",\"updated_at\":\"2015-09-03 19:21:52\"}},{\"id\":\"33\",\"name\":\"\u0421\u043c\u0435\u0448\u0430\u043d\u043d\u0430\u044f \u043a\u0443\u0445\u043d\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"20\",\"kitchen_id\":\"33\",\"created_at\":\"2015-09-03 19:21:52\",\"updated_at\":\"2015-09-03 19:21:52\"}}],\"services\":[{\"id\":\"7\",\"name\":\"\u0416\u0438\u0432\u0430\u044f \u043c\u0443\u0437\u044b\u043a\u0430\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"20\",\"service_id\":\"7\",\"created_at\":\"2015-09-03 19:21:52\",\"updated_at\":\"2015-09-03 19:21:52\"}},{\"id\":\"9\",\"name\":\"\u0417\u0430\u043b \u0434\u043b\u044f \u043d\u0435 \u043a\u0443\u0440\u044f\u0449\u0438\u0445\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"20\",\"service_id\":\"9\",\"created_at\":\"2015-09-03 19:21:52\",\"updated_at\":\"2015-09-03 19:21:52\"}},{\"id\":\"11\",\"name\":\"\u041a\u0430\u0440\u0430\u043e\u043a\u0435\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"20\",\"service_id\":\"11\",\"created_at\":\"2015-09-03 19:21:52\",\"updated_at\":\"2015-09-03 19:21:52\"}}]},{\"id\":\"19\",\"resto_name\":\"Gold dragon\",\"resto_desc\":\" Sushi restoraunt\r\n \",\"banner\":\"http:\/\/vignette1.wikia.nocookie.net\/pathfinder\/images\/2\/22\/Gold_dragon_head.jpg\",\"lon\":\"43.271183\",\"lat\":\"76.904945\",\"average_bills\":\"5000-7500\",\"contact_phone\":\"8 (747) 147-146\",\"work_in\":\"09:30:00\",\"created_at\":\"2015-08-04 18:21:44\",\"updated_at\":\"2015-09-03 19:21:15\",\"work_to\":\"09:30:00\",\"address\":\"Dostys st 152\",\"city\":\"almaty\",\"type\":\"\u0420\u0435\u0441\u0442\u043e\u0440\u0430\u043d\",\"district\":\"\u0410\u043b\u043c\u0430\u043b\u0438\u043d\u0441\u043a\u0438\u0439\",\"kitchens\":[{\"id\":\"24\",\"name\":\"\u041a\u0438\u0442\u0430\u0439\u0441\u043a\u0430\u044f \u043a\u0443\u0445\u043d\u044f\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"19\",\"kitchen_id\":\"24\",\"created_at\":\"2015-09-03 19:21:15\",\"updated_at\":\"2015-09-03 19:21:15\"}},{\"id\":\"40\",\"name\":\"\u042f\u043f\u043e\u043d\u0441\u043a\u0430\u044f \u043a\u0443\u0445\u043d\u044f \",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"19\",\"kitchen_id\":\"40\",\"created_at\":\"2015-09-03 19:21:15\",\"updated_at\":\"2015-09-03 19:21:15\"}}],\"services\":[{\"id\":\"6\",\"name\":\"\u0421\u0443\u0448\u0438 \u043c\u0435\u043d\u044e\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"19\",\"service_id\":\"6\",\"created_at\":\"2015-09-03 19:21:15\",\"updated_at\":\"2015-09-03 19:21:15\"}},{\"id\":\"7\",\"name\":\"\u0416\u0438\u0432\u0430\u044f \u043c\u0443\u0437\u044b\u043a\u0430\",\"created_at\":\"-0001-11-30 00:00:00\",\"updated_at\":\"-0001-11-30 00:00:00\",\"pivot\":{\"resto_id\":\"19\",\"service_id\":\"7\",\"created_at\":\"2015-09-03 19:21:15\",\"updated_at\":\"2015-09-03 19:21:15\"}}]}]";
}

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testExample {
    // This is an example of a functional test case.
    
    NSError *err = nil;
    NSData *data = [self.json dataUsingEncoding:NSUTF8StringEncoding];
    NSArray * jsonArray = [NSJSONSerialization
            JSONObjectWithData:data
            options:kNilOptions
            error:&err];
    
    NSMutableArray * restorauntsArray = [[NSMutableArray alloc] init];
    
    for (NSDictionary *dic in jsonArray){
        Restaurant * restaurant = [[Restaurant alloc] initWithDictionary:dic];
        [restorauntsArray addObject:restaurant];
    }
    
    NSLog(@"%@ restorauntsArray",restorauntsArray);
    
    XCTAssertNotNil(restorauntsArray);
//    XCTAssertEqual(restorauntsArray.count, 5);
//    XCTAssertEqual([restorauntsArray[0] restaurantId], @"32");

    
   
}

- (void)testPerformanceExample {
    // This is an example of a performance test case.
    [self measureBlock:^{
// Put the code you want to measure the time of here.
    }];
}

@end
