//
//  AppDelegate.h
//  restorapp
//
//  Created by Tamerlan Imanov on 17.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

