//
//  Pivot.h
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Pivot : NSObject

@property (strong,nonatomic) NSString * createdAt;
@property (strong,nonatomic) NSString * restoId;
@property (strong,nonatomic) NSString * updatedAt;

-(instancetype)initWithDictionary:(NSDictionary*) dictionary;

@end
