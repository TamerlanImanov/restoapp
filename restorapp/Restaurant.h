//
//  Restaurant.h
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Restaurant;

@interface Restaurant : NSObject

@property (strong,nonatomic) NSString * restaurantId;
@property (strong,nonatomic) NSString * restoName;
@property (strong,nonatomic) NSString *restoDesc;
@property (strong,nonatomic) NSString * lon;
@property (strong,nonatomic) NSString * lat;
@property (strong,nonatomic) NSString * averageBills;
@property (strong,nonatomic) NSString * contactPhone;
@property (strong,nonatomic) NSString * workIn;
@property (strong,nonatomic) NSString * workTo;
@property (strong,nonatomic) NSString * address;
@property (strong,nonatomic) NSString * city;
@property (strong,nonatomic) NSString * type;
@property (strong,nonatomic) NSString * district;
@property (strong,nonatomic) NSString * banner;

@property (strong,nonatomic) NSMutableArray * kitchens;
@property (strong,nonatomic) NSMutableArray * services;

-(instancetype)initWithDictionary:(NSDictionary*) dictionary;
-(BOOL) equal: (Restaurant*) target;

@end