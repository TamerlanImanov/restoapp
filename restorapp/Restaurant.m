//
//  Restaurant.m
//  restorapp
//
//  Created by Tamerlan Imanov on 04.09.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "Restaurant.h"
#import "Kitchen.h"
#import "Service.h"

@implementation Restaurant

-(instancetype)initWithDictionary:(NSDictionary*) dictionary{
    self = [super init];
    
    if (self) {
        self.restaurantId = dictionary[@"id"];
        self.restoName = dictionary[@"resto_name"];
        self.restoDesc = dictionary[@"resto_desc"];
        self.lon = dictionary[@"lon"];
        self.lat = dictionary[@"lat"];
        self.averageBills = dictionary[@"average_bills"];
        self.contactPhone = dictionary[@"contact_phone"];
        self.workIn = dictionary[@"work_in"];
        self.workTo = dictionary[@"work_to"];
        self.address = dictionary[@"address"];
        self.city = dictionary[@"city"];
        self.type = dictionary[@"type"];
        self.district = dictionary[@"district"];
        self.kitchens = [[NSMutableArray alloc] init];
        self.services = [[NSMutableArray alloc] init];
        
        for (NSDictionary * kitchenDic in dictionary[@"kitchens"]) {
            Kitchen * kitchen = [[Kitchen alloc] initWithDictionary:kitchenDic];
            [self.kitchens addObject:kitchen];
        }

        for (NSDictionary * serviceDic in dictionary[@"services"]) {
            Service * service = [[Service alloc] initWithDictionary:serviceDic];
            [self.services addObject:service];
        }
    }

    return self;
}

-(BOOL) equal: (Restaurant*) target{
    return [self.restaurantId isEqualToString:target.restaurantId];
}

@end
