//
//  PersonalViewController.m
//  restorapp
//
//  Created by Tamerlan Imanov on 19.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import "PersonalViewController.h"

@interface PersonalViewController ()
{
    NSMutableArray *busStopAnnotations;
}
@end

@implementation PersonalViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.mapView.delegate = self;
    NSString *lat =[NSString stringWithFormat:@"%@",self.personalDic[@"lat"]];
    NSString *lon =[NSString stringWithFormat:@"%@",self.personalDic[@"lon"]];
    float spanX = [lat floatValue];
    float spanY = [lon floatValue];
    [self.mapView setMapType:MKMapTypeStandard];
    
    CLLocationCoordinate2D center;
    center.latitude = spanY;
    center.longitude = spanX;
    MKCoordinateSpan span;
    span.latitudeDelta = .01f;
    span.longitudeDelta = .01f;
    MKCoordinateRegion region;
    region.center = center;
    region.span = span;
    [self.mapView setRegion:region animated:YES];
    
//    MKPointAnnotation *pointAnnotation =[[MKPointAnnotation alloc]init];
//    [pointAnnotation setCoordinate:center];
//    
    MKAnnotationView *annotationView = [[MKAnnotationView alloc]init];
    annotationView.enabled = YES;
    annotationView.image = [UIImage imageNamed:@"pin.png"];
    
//    MKPinAnnotationView *pinAnnotationView = [[MKPinAnnotationView  alloc]initWithAnnotation:pointAnnotation reuseIdentifier:@"op"];
//    pinAnnotationView.image = [UIImage imageNamed:@"pin.png"];

    
//    [annotationView addSubview:pointAnnotation];
//    [self.mapView addAnnotation:pointAnnotation];
    
    [self.mapView addAnnotation:annotationView];
    self.navigationItem.backBarButtonItem.title = @"Назад";
    
//    NSURL *URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",self.personalDic[@"banner"]]];
//    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
//        NSData *data = [NSData dataWithContentsOfURL:URL];
//        if(data){
//            dispatch_async(dispatch_get_main_queue(), ^{
                self.personalImage.image = self.personalImageGet;
//            });
//        }
//    });
    self.scrollView.scrollEnabled = YES;
    self.personalDesc.scrollEnabled = NO;
    self.personalDesc.textColor = [UIColor lightGrayColor];
    self.personalName.text = [NSString stringWithFormat:@"%@",self.personalDic[@"resto_name"]];
    self.personalAddress.text = [NSString stringWithFormat:@"%@",self.personalDic[@"address"]];
    self.personalKitchen.text = [NSString stringWithFormat:@"%@",self.personalDic[@"kitchen"]];
    self.personalBills.text = [NSString stringWithFormat:@"Средний счет: %@тг",self.personalDic[@"average_bills"]];
    self.personalDesc.text = [NSString stringWithFormat:@"%@",self.personalDic[@"resto_desc"]];
    self.personalPhoneLabel.text = [NSString stringWithFormat:@"%@",self.personalDic[@"contact_phone"]];
    NSString *str = [NSString stringWithFormat:@"%@",self.personalDic[@"work_in"]];
    str = [self checkTime:str];
    NSString *str2 = [NSString stringWithFormat:@"%@",self.personalDic[@"work_to"]];
    str2 = [self checkTime:str2];
    self.personalWork.text = [NSString stringWithFormat:@"С %@ до %@",str,str2];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSString*)checkTime:(NSString*)str{
    if ([[str substringWithRange:NSMakeRange(0,1)] integerValue] <1){
        str = [str substringWithRange:NSMakeRange(1,4)];
    }else{
        str = [str substringWithRange:NSMakeRange(0,5)];
    }
    return str;
}

-(NSString*)checkPhone:(NSString*)str{
    NSCharacterSet *doNotWant = [NSCharacterSet characterSetWithCharactersInString:@"+().:- "];
    str = [[str componentsSeparatedByCharactersInSet: doNotWant] componentsJoinedByString: @""];
    return str;
}

- (IBAction)callAction:(id)sender {
    NSString *phoneNumber = [self checkPhone:[NSString stringWithFormat:@"%@",self.personalDic[@"contact_phone"]]];
    NSString *tel = [NSString stringWithFormat:@"tel:+%@",phoneNumber];
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:tel]];
}

@end