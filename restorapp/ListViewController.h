//
//  ListViewController.h
//  restorapp
//
//  Created by Tamerlan Imanov on 18.08.15.
//  Copyright (c) 2015 msg.kz. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController <UITableViewDataSource,UITableViewDelegate,NSURLConnectionDataDelegate>

@property (nonatomic) NSUInteger totalBytes;
@property (nonatomic) NSUInteger receivedBytes;
@property (strong, nonatomic) NSMutableArray * finalArray;

@property (weak, nonatomic) IBOutlet UIButton *restBut;
@property (weak, nonatomic) IBOutlet UIButton *cafeBut;
@property (weak, nonatomic) IBOutlet UIButton *coffeeBut;
@property (weak, nonatomic) IBOutlet UIButton *barBut;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *whiteView;
@property (strong,nonatomic) NSMutableArray * restorArray;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (strong,nonatomic) NSMutableArray * restorID;
@property (strong,nonatomic) NSMutableArray *resto_name;
@property (strong,nonatomic) NSMutableArray *resto_desc;
@property (strong,nonatomic) NSMutableArray *banner;
@property (strong,nonatomic) NSMutableArray *lon;
@property (strong,nonatomic) NSMutableArray *lat;
@property (strong,nonatomic) NSMutableArray *average_bills;
@property (strong,nonatomic) NSMutableArray *contact_phone;
@property (strong,nonatomic) NSMutableArray *work_in;
@property (strong,nonatomic) NSMutableArray *work_to;
@property (strong,nonatomic) NSMutableArray *address;
@property (strong,nonatomic) NSMutableArray *city;
@property (strong,nonatomic) NSMutableArray *type;
@property (strong,nonatomic) NSMutableArray *district;
@property (strong,nonatomic) NSMutableArray *kitchen;

@end
